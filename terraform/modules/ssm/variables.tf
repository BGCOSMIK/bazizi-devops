variable "POSTGRES_PASSWORD" {
  type        = string
}

variable "POSTGRES_PORT" {
  type        = string
}

variable "POSTGRES_HOST" {
  type        = string
}
variable "POSTGRES_USER" {
  type        = string
}

variable "GITHUB_TOKEN" {
  type        = string
}

variable "project" {
  type        = string
}

