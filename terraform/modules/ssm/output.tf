output "postgres_password_arn" {
  value = aws_ssm_parameter.POSTGRES_PASSWORD.arn
}

output "postgres_user_arn" {
  value = aws_ssm_parameter.POSTGRES_USER.arn
}

output "postgres_port_arn" {
  value = aws_ssm_parameter.POSTGRES_PORT.arn
}

output "postgres_host_arn" {
  value = aws_ssm_parameter.POSTGRES_HOST.arn
}
output "github_token_arn" {
  value = aws_ssm_parameter.GITHUB_TOKEN.arn
}
