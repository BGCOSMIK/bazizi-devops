resource "aws_ssm_parameter" "POSTGRES_PORT" {
  name  = "POSTGRES_HOST"
  type  = "String"
  value = var.POSTGRES_PORT
}

resource "aws_ssm_parameter" "POSTGRES_PASSWORD" {
  name     = "POSTGRES_PASSWORD"
  type     = "SecureString"
  value    = var.POSTGRES_PASSWORD
  overwrite = true
}
resource "aws_ssm_parameter" "POSTGRES_USER" {
  name     = "POSTGRES_USER"
  type     = "SecureString"
  value    = var.POSTGRES_USER
  overwrite = true
}

resource "aws_ssm_parameter" "POSTGRES_HOST" {
  name     = "POSTGRES_HOST"
  type     = "String"
  value    = var.POSTGRES_HOST
  overwrite = true
}


resource "aws_ssm_parameter" "GITHUB_TOKEN" {
  name     = "GITHUB_TOKEN"
  type     = "SecureString"
  value    = var.GITHUB_TOKEN
  overwrite = true
}


