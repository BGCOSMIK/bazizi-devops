resource "aws_ecs_cluster" "default_cluster" {
  name = "${var.project}-cluster"
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_cloudwatch_log_group" "default_log_group" {
  name = "/ecs/${var.project}"
}


resource "aws_ecs_task_definition" "default_task" {
  family                   = "${var.project}-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 1024
  execution_role_arn       = var.execution_role_arn
  container_definitions    = jsonencode([{
    name        = "${var.project}-container"
    image       = "${var.docker_image_url}:${var.docker_image_tag}"
    essential   = true
    secrets: [
      {"name": "POSTGRES_HOST", "valueFrom": var.postgres_host_arn},
      {"name": "POSTGRES_USER", "valueFrom": var.postgres_user_arn},
      {"name": "POSTGRES_PASSWORD", "valueFrom": var.postgres_password_arn},
      {"name": "GITHUB_TOKEN", "valueFrom": var.github_token_arn}
    ]
    environment: [
      {"name": "APP_DOMAIN", "value": "http://${var.alb_dns_name}"},
      {"name": "APP_URL", "value": "http://${var.alb_dns_name}"},
      {"name": "BACKEND_URL", "value": "http://${var.alb_dns_name}"},
      {"name": "POSTGRES_PORT", "value": "5432"},
      {"name": "DEFAULT_REGION", "value": var.default_region},
      {"name": "BUCKET_NAME", "value": var.tech_docs_bucket_name}
    ],
    logConfiguration = {
    logDriver = "awslogs"
    options: {
      "awslogs-group": "/ecs/${var.project}",
      "awslogs-region": var.default_region,
      "awslogs-stream-prefix": "ecs"
    }
    }
    portMappings = [{
    protocol      = "tcp"
    containerPort = 7007
    hostPort      = 7007
    }]
  }])
  tags = {
    Name = "${var.project}-task"
  }
}


resource "aws_ecs_service" "default_service" {
  name                               = "${var.project}-service"
  cluster                            = aws_ecs_cluster.default_cluster.id
  task_definition                    = aws_ecs_task_definition.default_task.arn
  launch_type                        = "FARGATE"
  platform_version                   = "1.4.0"
  desired_count                      = 1
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  scheduling_strategy                = "REPLICA"
  depends_on                         = [aws_ecs_task_definition.default_task]

  network_configuration {
    security_groups  = var.security_group_ids
    subnets                    = var.subnet_ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.target_group_arn
    container_name   = "${var.project}-container"
    container_port   = 7007
  }
} 

resource "aws_cloudwatch_metric_alarm" "ecs_scaling_alarm" {
  alarm_name          = "${var.project}-ecs-scaling-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 70
  alarm_description   = "Alarm when CPU exceeds 70%"
  dimensions = {
    ClusterName = aws_ecs_cluster.default_cluster.name
    ServiceName = aws_ecs_service.default_service.name
  }
  alarm_actions       = [aws_appautoscaling_policy.ecs_scaling_policy.arn]
}

resource "aws_appautoscaling_target" "ecs_scaling_target" {
  max_capacity       = 4
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.default_cluster.name}/${aws_ecs_service.default_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_scaling_policy" {
  name               = "${var.project}-ecs-scaling-policy"
  service_namespace  = "ecs"
  scalable_dimension = "ecs:service:DesiredCount"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_scaling_target.resource_id

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
    target_value       = 70
  }
}

