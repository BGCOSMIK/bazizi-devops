variable "project" {
  description = "The name of the project"
}

variable "docker_image_url" {
  type        = string
  description = "The URL of the Docker image"
}

variable "docker_image_tag" {
  type        = string
  description = "The tag of the Docker image"
}

variable "execution_role_arn" {
  type        = string
  description = "The ARN of the execution role"
}

variable "postgres_host_arn" {
  type        = string
  description = "The ARN of the parameter for PostgreSQL host"
}

variable "postgres_user_arn" {
  type        = string
  description = "The ARN of the parameter for PostgreSQL user"
}

variable "postgres_password_arn" {
  type        = string
  description = "The ARN of the parameter for PostgreSQL password"
}

variable "github_token_arn" {
  type        = string
  description = "The ARN of the parameter for GitHub token"
}

variable "alb_dns_name" {
  type        = string
  description = "The DNS name of the Application Load Balancer (ALB)"
}

variable "default_region" {
  type        = string
  description = "The default region"
}

variable "tech_docs_bucket_name" {
  type        = string
  description = "The name of the S3 bucket for technical documentation"
}

variable "security_group_ids" {
  type        = list(string)
  description = "The IDs of the security groups"
}

variable "target_group_arn" {
  type        = string
  description = "The ARN of the target group"
}

variable "subnet_ids" {}