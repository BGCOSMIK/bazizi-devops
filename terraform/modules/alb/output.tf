output "alb_arn" {
  description = "ARN de l'Application Load Balancer créé"
  value       = aws_alb.default_alb.arn
}

output "alb_dns_name" {
  description = "Nom DNS de l'Application Load Balancer créé"
  value       = aws_alb.default_alb.dns_name
}

output "target_group_arn" {
  description = "ARN du Target Group créé pour l'ALB"
  value       = aws_alb_target_group.default_tg.arn
}

output "listener_arn" {
  description = "ARN du Listener créé pour l'ALB"
  value       = aws_alb_listener.default_listener.arn
}

output "default_security_group_id" {
  value = aws_security_group.default_sg.id
}