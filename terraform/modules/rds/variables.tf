variable "project" {
  description = "Nom du projet"
}

variable "storage" {
  description = "Espace de stockage alloué pour la base de données (en GB)"
}

variable "rds_username" {
  description = "Nom d'utilisateur de la base de données"
}

variable "rds_password" {
  description = "Mot de passe de la base de données"
}

variable "default_security_group_id" {
  description = "ID du groupe de sécurité par défaut pour l'accès à la base de données RDS"
}

variable "rds_security_group_id" {
  description = "ID du groupe de sécurité RDS"
}

variable "vpc_id" {
  description = "The ID of the VPC where the RDS instance will be launched"
  type        = string
}

variable "rds_subnets" {
  type = map(string)
}
