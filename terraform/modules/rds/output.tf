output "db_instance_id" {
  value = aws_db_instance.default_db.id
}

output "db_instance_endpoint" {
  value = aws_db_instance.default_db.endpoint
}

output "db_instance_username" {
  value = aws_db_instance.default_db.username
}

output "db_instance_password" {
  value = aws_db_instance.default_db.password
}

output "rds_security_group_id" {
  value = aws_security_group.rds_instance_sg.id
}
