resource "aws_ecr_repository" "registry" {
  name               = "${var.project}-images"
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }
}