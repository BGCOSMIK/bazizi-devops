variable "scan_on_push" {
  description = "Whether to scan images on push"
  type        = bool
  default     = false
}

variable "project" {
  description = "The name of the repository"
}
