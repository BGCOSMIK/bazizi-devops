variable "name" {
  description = "The name of the S3 bucket"
  type        = string
}

variable "acl" {
  description = "The ACL (Access Control List) for the S3 bucket"
  type        = string
}

variable "versioning" {
  description = "Whether versioning is enabled for the S3 bucket"
  type        = bool
}

variable "project" {
  description = "The name of the S3 bucket"
  type        = string
}