output "s3_bucket_name" {
  value = aws_s3_bucket.tech_docs_bucket.bucket
}

output "s3_bucket_arn" {
  value = aws_s3_bucket.tech_docs_bucket.arn
}

output "s3_bucket_domain_name" {
  value = aws_s3_bucket.tech_docs_bucket.bucket_domain_name
}

output "s3_bucket_region" {
  value = aws_s3_bucket.tech_docs_bucket.region
}

output "s3_bucket_access_block_id" {
  value = aws_s3_bucket_public_access_block.tech_docs_bucket_acl.id
}
