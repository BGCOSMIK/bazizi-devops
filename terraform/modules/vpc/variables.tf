variable "project" {}
variable "default_region" {}
variable "vpc_cidr_block" {}
variable "public_subnets" {
  type = map(string)
}

variable "rds_subnets" {
  type = map(string)
}