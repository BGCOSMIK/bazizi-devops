variable "project" {}
variable "default_region" {}
variable "vpc_cidr_block" {}

variable "rds_subnets" {
  type = map(string)
}
variable "public_subnets" {
  type = map(string)
}

variable "rds_username" {
  description = "Username for the RDS instance"
}

variable "rds_password" {
  description = "Password for the RDS instance"
}

variable "GITHUB_TOKEN" {}
variable "BUCKET_NAME" {}