terraform {
  required_version = "~> 1.7.5"

  backend "s3" {
    bucket = "backstage-devops"
    key    = "tf-states/environment/terraform.tfstate"
    region = "us-east-1"
    workspace_key_prefix = "environment"
  }  

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.44.0"
    }
  }
}