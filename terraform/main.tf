provider "aws" {
  region = "us-east-1"
}

module "aws_vpc" {
  source          = "./modules/vpc"
  default_region  = var.default_region
  project         = var.project
  vpc_cidr_block = var.vpc_cidr_block
  public_subnets = var.public_subnets
  rds_subnets    = var.rds_subnets
}

module "aws_alb"{
  source          = "./modules/alb"
  project         = var.project
  vpc_id          = module.aws_vpc.vpc_id
  subnet_ids      = values(module.aws_vpc.public_subnets)
}

module "aws_rds" {
  source                = "./modules/rds"
  project               = var.project
  storage               = 20
  rds_username              = var.rds_username
  rds_password              = var.rds_password
  vpc_id                = module.aws_vpc.vpc_id
  default_security_group_id = module.aws_alb.default_security_group_id
  rds_subnets = module.aws_vpc.rds_subnets
  rds_security_group_id = module.aws_rds.rds_security_group_id
  depends_on = [module.aws_vpc,module.aws_alb]
}

module "aws_s3"{
  source = "./modules/s3"
  project = var.project
  acl = "private"
  versioning = false
  name = "backstage-devops"
}

module "aws_ssm_parameter" {
  source = "./modules/ssm"
  project = var.project
  POSTGRES_HOST = module.aws_rds.db_instance_endpoint
  POSTGRES_PORT = 5432
  POSTGRES_USER = var.rds_username
  POSTGRES_PASSWORD = var.rds_password
  GITHUB_TOKEN = var.GITHUB_TOKEN
}

module "aws_ecr_repository"{
  source = "./modules/ecr"
  project            = var.project
  scan_on_push       = false

}
module "ecs" {
  source = "./modules/ecs"
  project                     = var.project
  docker_image_url            = module.aws_ecr_repository.ecr_repository_url
  docker_image_tag            = "latest"
  execution_role_arn          = "arn:aws:iam::467460485538:role/LabRole"
  postgres_host_arn           = module.aws_ssm_parameter.postgres_host_arn
  postgres_user_arn           = module.aws_ssm_parameter.postgres_user_arn
  postgres_password_arn       = module.aws_ssm_parameter.postgres_password_arn
  github_token_arn            = module.aws_ssm_parameter.github_token_arn
  alb_dns_name                = module.aws_alb.alb_dns_name
  default_region              = var.default_region
  tech_docs_bucket_name       = module.aws_s3.s3_bucket_name
  security_group_ids          = [module.aws_alb.default_security_group_id]
  subnet_ids                  = values(module.aws_vpc.public_subnets)
  target_group_arn            = module.aws_alb.target_group_arn

}
