# Bazizi Devops

# Pour set-up l'application il faut yarn v1 et node 18

1. `cd backstaging`
2. `yarn install`

# Pour set-up le docker il faut installer docker desktop

Depuis le répertoire backstaging :

1. `yarn install --frozen-lockfile`
2. `yarn tsc`
3. `yarn build:backend --config ../../app-config.yaml`
4. `docker image build . -f packages/backend/Dockerfile --tag backstage`

# Pour set-up le terraform

1. Créer un bucket S3 nommé "backstage-devops"
2. Installer Terraform
3. Installer AWS CLI
4. Copier les informations d'identification AWS lorsque vous lancez AWS et collez-les dans `~/.aws/credentials` (vous trouverez le fichier dans "AWS details")
5. Dans le main.tf ligne  63 j'ai mis   `execution_role_arn          = "arn:aws:iam::467460485538:role/LabRole"` il faudrait changer 467460485538 par votre numéro de compte si vous utilisez votre compte
6. Exécuter les commandes suivantes :
    - `terraform init`
    - `terraform plan`
    - `terraform apply`

Pour détruire l'infrastructure, exécuter :
`terraform destroy`


